<?php

namespace FormantaBlocks;


use FormantaBlocks\Config\View;

class Config {
    public $url = [];
    /**
     * @var View
     */
    public $view;

    protected $config = [];

    public function __construct($config) {
        if (isset($config['view']) && is_array($config['view'])) {
            $this->view = new View($config['view']);
        } else {
            $this->view = new View(false);
        }

        if (isset($config['url']) && is_array($config['url'])) {
            $this->url = $config['url'];
        }
    }
}