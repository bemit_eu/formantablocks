<?php

namespace FormantaBlocks;

class Renderer {
    /**
     * Data that should be inserted the template
     *
     * @var array
     */
    protected $tpl_data = [];
    /**
     * @var \Twig_Loader_Filesystem
     */
    public $twig_loader = null;
    /**
     * @var \Twig_Environment
     */
    public $twig_env = null;

    /**
     * Sets config and initializes twig, adds the needed view paths from config
     *
     * @param Config\View $config
     */
    public function __construct($config) {
        /**
         * getting the first folder from the path_view array, this is the default view folder
         */
        $tmp_arr = $config->storeView();
        $this->twig_loader = new \Twig_Loader_Filesystem(array_shift($tmp_arr));
        unset($tmp_arr);

        foreach ($config->storeView() as $key => $value) {
            try {
                if (is_string($key)) {
                    // key = path, value = namespace
                    $this->twig_loader->addPath($key, $value);
                } else {
                    // value = path, no namespace
                    $this->twig_loader->addPath($value);
                }
            } catch (\Twig_Error_Loader $e) {
                echo $e->getMessage() . "\r\n";
            }
        }

        $this->twig_env = new \Twig_Environment($this->twig_loader, [
            'debug'       => $config->debug(),
            'auto_reload' => $config->autoReload(),
            'cache'       => $config->storeCache(),
        ]);

        if ($config->debug()) {
            // make dump() and more available
            $this->twig_env->addExtension(new \Twig_Extension_Debug());
        }
    }

    /**
     * Fluent interface return
     *
     * @param $key
     * @param $val
     *
     * @return $this
     */
    public function assign($key, $val) {
        $this->tpl_data[$key] = $val;
        return $this;
    }

    public function assignByRef($key, &$val) {
        $this->tpl_data[$key] = &$val;
    }

    /**
     * Renders the template and injects the values into the global template data
     *
     * @param       $template
     * @param array $value
     *
     * @return string
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function render($template, $value = []) {
        return $this->twig_env->load($template)->render(\Flood\Component\Func\Array_::merge_recursive_distinct($this->tpl_data, $value));
    }
}