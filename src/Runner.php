<?php

namespace FormantaBlocks;

/**
 * Main Runner class, contains the static generator and in-time API
 *
 * @package FormantaBlocks
 */
class Runner {
    public $config = [];

    public $static_gen;

    protected $header = [];

    protected static $msg_send = false;

    /**
     * Sets configs along with the static generator
     *
     * @param Config $config
     */
    public function __construct($config) {
        $this->config = $config;

        $this->static_gen = new StaticGenerator($this->config->view);
    }

    public function sendHeader() {
        if (false === static::$msg_send) {
            foreach ($this->header as $h) {
                header($h, true);
            }
        } else {
            error_log('FormantaBlocks: sendHeader: message was already send, can not send header.');
            error_log(json_encode(debug_backtrace()));
        }
    }

    public function addHeader($header, $as_array = false) {
        if ($as_array) {
            $this->header = array_merge($this->header, $header);
        } else {
            $this->header[] = $header;
        }
    }

    public function sendMessage(&$msg) {
        static::$msg_send = true;
        echo $msg;
    }
}