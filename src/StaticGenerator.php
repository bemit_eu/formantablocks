<?php

namespace FormantaBlocks;

use Flood\Component\PerformanceMonitor\Monitor;

/**
 * Class StaticGenerator
 *
 * @package FormantaBlocks
 * @todo    template system could handle multiple view dirs, this class doesn't do everything correctly on namespaced classes
 */
class StaticGenerator {

    /**
     * @var \FormantaBlocks\Config\View
     */
    public $config;

    public $renderer;

    /**
     * @var array
     */
    protected $builded;

    /**
     * Sets configs and checks build_info_file existence, creates needed renderer
     *
     * @param Config\View $config
     */
    public function __construct($config) {
        $this->config = $config;

        if(!file_exists($this->config->storeBuildedInfo())) {
            file_put_contents($this->config->storeBuildedInfo(), json_encode([]), LOCK_EX);
        } else {
            if(is_file($this->config->storeBuildedInfo())) {
                $this->builded = json_decode(file_get_contents($this->config->storeBuildedInfo()), true);
            } else {
                $this->builded = false;
            }
            if(!is_array($this->builded)) {
                error_log('FormantaBlocks: could not retrieve builded json, please see file: `' . $this->config->storeBuildedInfo() . '`, cleaning will not be possible, check yourself old static html files.');
                $this->builded = [];
            }
        }

        $this->renderer = new Renderer($this->config);
    }

    /**
     * Renders the template to an static html file, injects template dependent values from a json file, uses all other values defined in the templates-global tpl_data
     *
     * @param string $id unique id, will be used also as filename to the value json of the template file
     * @param array  $info
     *
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function render($id, $info) {
        Monitor::i()->startProfile('formanta-blocks--static-generator--render');

        if(isset($info['view']) && $info['static']) {
            $value_file = $this->config->storeData() . $id . '.json';

            $value = [];

            if(is_file($value_file)) {
                $value = json_decode(file_get_contents($value_file), true);
            }

            $content = $this->renderer->render($info['view'], $value);

            if(is_string($content) && !empty($content)) {
                file_put_contents($this->config->storeBuild() . $info['static'], $content, LOCK_EX);
            }

            //todo check if really written everything;
            $this->writeToBuilded($id, $info['static']);
        }
        Monitor::i()->endProfile('formanta-blocks--static-generator--render');
        error_log(
            'FormantaBlocks: rendered: `' . $id . '` - `' . $info['view'] . '` to `' . $info['static'] . '` in ' .
            Monitor::i()->getInformation('formanta-blocks--static-generator--render')['time'] . 's' . ' ' .
            Monitor::i()->convertMemory(Monitor::i()->getInformation('formanta-blocks--static-generator--render')['memory']));
    }

    /**
     * Saves
     *
     * @param $id
     * @param $static
     */
    public function writeToBuilded($id, $static) {
        if(in_array($id, $this->builded) && $static !== $this->builded[$id][$static]) {
            // when the id is already in the array but the new static file is not the same like the old, the old file is no longer needed and should be deleted
            $this->delete($this->builded[$id], 'static');
        }
        $this->builded[$id] = $static;
        file_put_contents($this->config->storeBuildedInfo(), json_encode($this->builded), LOCK_EX);
    }

    /**
     * Deletes an static file and removes it when in the saved already builded templates (e.g. previous builds)
     *
     * @param bool   $id
     * @param bool   $static
     * @param bool   $view_file
     * @param string $type
     * @param bool   $verbose
     */
    public function invalidate($id = false, $static = false, $view_file = false, $type = 'unkown', $verbose = false) {
        $deleted = false;
        $changed = false;
        $id_changed = false;

        if(false !== $id) {
            if(isset($this->builded[$id])) {
                // when the id is already in the array but the new static file is not the same like the old, the old file is no longer needed and should be deleted
                $deleted = $this->delete($this->builded[$id], $type, $verbose);
                unset($this->builded[$id]);
                $id_changed = $id;
                $changed = true;
            }
        }

        if(false !== $static) {
            foreach($this->builded as $in_id => $stat) {
                if(0 === strpos($static, $this->config->storeBuild())) {
                    // when a function submitted a full length file
                    $static = str_replace(realpath($this->config->storeBuild()), '', ($static));
                }

                if($stat === $static) {
                    // when the id is already in the array but the new static file is not the same like the old, the old file is no longer needed and should be deleted
                    $deleted = $this->delete($stat, $type, $verbose);
                    unset($this->builded[$in_id]);
                    $id_changed = $in_id;
                    $changed = true;
                    break;
                }
            }
        }

        if(false !== $view_file) {
            $build_target = $this->config->buildTarget();

            foreach($build_target as $build_id => $build_info) {
                // todo especially check here for multiple view dir support
                foreach($this->config->storeView() as $key => $value) {
                    if(is_string($key)) {
                        // key = path, value = namespace
                        $path = $key;
                    } else {
                        // value = path, no namespace
                        $path = $value;
                    }
                    if(realpath($path . '/' . $build_info['view']) === $view_file) {
                        $deleted = $this->delete($build_info['static'], $type, $verbose);
                        unset($this->builded[$build_id]);
                        $id_changed = $build_id;
                        $changed = true;
                        break;
                    }
                }
            }
        }

        if($deleted && $changed) {
            file_put_contents($this->config->storeBuildedInfo(), json_encode($this->builded), LOCK_EX);
        }
        return $id_changed;
    }

    /**
     * Deletes all already builded files
     *
     * @param bool $verbose error messages are printed no matter how but success messages only when verbose
     */
    public function clean($verbose = false) {
        foreach($this->builded as $id => $static) {
            $this->invalidate($id, false, false, 'static', $verbose);
        }
    }

    /**
     * @param        $file
     * @param string $type         is just for error message
     * @param bool   $show_success shows also success messages when true
     *
     * @return true
     */
    protected function delete($file, $type = '', $show_success = false) {
        if(!@unlink($this->config->storeBuild() . $file)) {
            error_log('FormantaBlocks: can not delete `' . $type . '` - file: `' . $file . '`');
            return false;
        } else {
            if($show_success) {
                error_log('FormantaBlocks: deleted `' . $type . '` - file: `' . $file . '`');
            }
            return true;
        }
    }

    /**
     * Build all files defined in the config
     *
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function build() {
        foreach($this->config->buildTarget() as $build_id => $build_info) {
            $this->render($build_id, $build_info);
        }
    }
}