<?php

namespace FormantaBlocks\Config;

class View {
    protected $config = [];

    public function __construct($config) {
        if(is_array($config)) {
            $this->config = $config;
        } else {
            $this->config = [];
        }
    }

    /**
     * @return array
     */
    public function buildTarget() {
        if(isset($this->config['build'])) {
            return $this->config['build'];
        } else {
            error_log('Config\View: build not set.');
            return [];
        }
    }

    /**
     * @return bool
     */
    public function autoReload() {
        if(isset($this->config['auto_reload'])) {
            return $this->config['auto_reload'];
        } else {
            error_log('Config\View: auto_reload not set.');
            return true;
        }
    }

    /**
     * @return bool
     */
    public function debug() {
        if(isset($this->config['debug'])) {
            return $this->config['debug'];
        } else {
            error_log('Config\View: debug not set.');
            return true;
        }
    }

    /**
     * @return string
     */
    public function storeCache() {
        if(isset($this->config['store']['cache_dir'])) {
            return $this->config['store']['cache_dir'];
        } else {
            error_log('Config\View: store.cache_dir not set.');
            return '';
        }
    }

    /**
     * @return array
     */
    public function storeView() {
        if(isset($this->config['store']['view_list'])) {
            return $this->config['store']['view_list'];
        } else {
            error_log('Config\View: store.view_list not set.');
            return [];
        }
    }

    /**
     * @return array
     */
    public function storeData() {
        if(isset($this->config['store']['data_dir'])) {
            return $this->config['store']['data_dir'];
        } else {
            error_log('Config\View: store.data_dir not set.');
            return [];
        }
    }

    /**
     * @return string
     */
    public function storeBuildedInfo() {
        if(isset($this->config['store']['builded_info_file'])) {
            return $this->config['store']['builded_info_file'];
        } else {
            error_log('Config\View: store.builded_info_file not set.');
            return '';
        }
    }

    /**
     * @return string
     */
    public function storeBuild() {
        if(isset($this->config['store']['build_dir'])) {
            return $this->config['store']['build_dir'];
        } else {
            error_log('Config\View: store.build_dir not set.');
            return '';
        }
    }
}