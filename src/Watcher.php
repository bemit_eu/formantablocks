<?php

namespace FormantaBlocks;

use \Illuminate\Filesystem\Filesystem;
use \JasonLewis\ResourceWatcher\Tracker;
use \JasonLewis\ResourceWatcher\Watcher as _Watcher;

class Watcher extends _Watcher {

    static public $_file = null;
    static public $_tracker = null;

    public function __construct() {
        if (null === static::$_file) {
            static::$_file = new Filesystem();
        }
        if (null === static::$_tracker) {
            static::$_tracker = new Tracker();
        }

        parent::__construct(static::$_tracker, static::$_file);
    }
}